/**
 * Created by ideog on 13.01.2018.
 * @author ideogaib@gmail.com
 */

class Marvelmind {
    private static final int BUFSIZE = 256;
    private static final int PDGSIZE = 29;
    private static final short PDGHEADER[] = {0xff, 0x47, 0x11, 0x00, 0x16};

    public Vector5 last_linear_values = new Vector5();
    public Vector5 old_last_linear_values = new Vector5();
    public int isButtonPressed = 0;

    private short[] positionDatagram = new short[PDGSIZE];
    private byte cycle_buffer[] = new byte[BUFSIZE * 32];
    private int cycle_buffer_end = 0;
    private int cycle_buffer_start = 0;
    private int pdgEnd = 0;
    private int left_hedge_addr = 95;
    private int right_hedge_addr = 96;


    public int modRTU_CRC(short[] buf, int len) {
        int crc = 0xffff;
        for (int ind = 0; ind < len; ind++) {
            crc ^= (int) buf[ind] & 0xff;
            for (int i = 8; i != 0; i--) {
                if ((crc & 0x0001) != 0) {
                    crc >>= 1;
                    crc ^= 0xA001;
                } else crc >>= 1;
            }
        }
        return crc;
    }

    public int findFrame(byte[] data) {
        if (data != null && data.length > 0) {
            int numBytesRead = data.length;
            if (numBytesRead > 0) {
                for (int k = 0; k < numBytesRead; k++) {
                    cycle_buffer[k + cycle_buffer_end] = data[k];
                }

                cycle_buffer_end += numBytesRead;
                int local_cycle_start = cycle_buffer_start;
                for (int j = local_cycle_start + 1; j < cycle_buffer_end; j++) {
                    if ((cycle_buffer[j - 1] == (byte) (0xff)) && (cycle_buffer[j] == (byte) (0x47))) {
                        pdgEnd = 0;
                        cycle_buffer_start = j - 1;
                        for (int k = cycle_buffer_start; k < cycle_buffer_end; k++) {
                            pdgEnd += 1;
                            if (pdgEnd == PDGSIZE) {
                                for (int l = cycle_buffer_start; l < cycle_buffer_start + pdgEnd; l++) {
                                    positionDatagram[l - cycle_buffer_start] = (short) (cycle_buffer[l] & 0xff);
                                }
                                parseFrame(positionDatagram);
                                break;
                            }
                        }
                    }
                }
                if ((cycle_buffer_start < (numBytesRead - PDGSIZE)) || (cycle_buffer_end + BUFSIZE > cycle_buffer.length)) {
                    cycle_buffer_end = 0;
                    cycle_buffer_start = 0;
                }
            }
            return 1;
        }
        return 0;
    }


    public boolean parseFrame(short[] frame) {
        for (int i = 0; i < PDGHEADER.length; i++) {
            if (frame[i] != PDGHEADER[i]) {
                return false;
            }
        }

        int crc = frame[27] | (frame[28] << 8);
        if (modRTU_CRC(frame, PDGSIZE - 2) != crc) return false;

        old_last_linear_values.set(last_linear_values);

        last_linear_values.t = frame[5] |
                (frame[6] << 8) |
                (frame[7] << 16) |
                (frame[8] << 24);

        last_linear_values.addr = frame[22];

        last_linear_values.v.x = frame[9] |
                (frame[10] << 8) |
                (frame[11] << 16) |
                (frame[12] << 24);

        last_linear_values.v.y = frame[13] |
                (frame[14] << 8) |
                (frame[15] << 16) |
                (frame[16] << 24);

        last_linear_values.v.z = frame[17] |
                (frame[18] << 8) |
                (frame[19] << 16) |
                (frame[20] << 24);

        isButtonPressed = ((((int) frame[21]) << 2) & 0xff) >> 7;

        if (old_last_linear_values.t == last_linear_values.t) return false;
        return (last_linear_values.addr == left_hedge_addr) || (last_linear_values.addr == right_hedge_addr);
    }
}
